//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Redmond Militante on 4/27/12.
//  Copyright (c) 2012 Kraftwerking, LLC. All rights reserved.
//

#import "CalculatorViewController.h"

@implementation CalculatorViewController

- (CalculatorBrain *)brain
{
    if (!brain) brain = [[CalculatorBrain alloc] init];
    return brain; 
}
    
- (IBAction)digitPressed:(UIButton *)sender
{
    NSString *digit = [[sender titleLabel] text];
    NSRange range = [[display text] rangeOfString:@"."];
    if (userIsInTheMiddleOfTypingANumber) 
    {
        if ( ! ([digit isEqual:@"."] && (range.location != NSNotFound))) {
            [display setText:[[display text] stringByAppendingFormat:digit]];
        }
    }
    else 
    {
        if ([digit isEqual:@"."]) {
            [display setText: @"0."];
        }
        else {
            [display setText: digit];
        }
        
        userIsInTheMiddleOfTypingANumber = YES;
        
    }
}

- (IBAction)clearPressed:(UIButton *)sender
{
    display.text = @"0";
    userIsInTheMiddleOfTypingANumber = NO;
    brain = nil;
}


- (IBAction)operationPressed:(UIButton *)sender
{
    if (userIsInTheMiddleOfTypingANumber) {
        [[self brain] setOperand:[[display text] doubleValue]];
        userIsInTheMiddleOfTypingANumber = NO;
    }
    
    //NSLog(@"The answer to %@, the universe and everything is %d.", @"life", 42);
    NSString *operation = [[sender titleLabel] text];
    double result = [[self brain] performOperation:operation];
    [display setText:[NSString stringWithFormat:@"%g", result]];
    
}


@end
