//
//  CalculatorBrain.h
//  Calculator
//
//  Created by Redmond Militante on 4/27/12.
//  Copyright (c) 2012 Kraftwerking, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject { 
    double operand;
    NSString *waitingOperation;
    double waitingOperand;
    double storedOperand;
}

- (void)setOperand:(double)aDouble;
- (void)performWaitingOperation;
- (double)performOperation:(NSString *)operation;

@end