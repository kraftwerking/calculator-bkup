//
//  CalculatorBrain.m
//  Calculator
//
//  Created by Redmond Militante on 4/27/12.
//  Copyright (c) 2012 Kraftwerking, LLC. All rights reserved.
//

#import "CalculatorBrain.h"
#import <math.h>

@implementation CalculatorBrain

- (void)setOperand:(double)aDouble
{ 
    operand = aDouble;
}

- (double)performOperation:(NSString *)operation
{
    if ([operation isEqual:@"sqrt"])
    {
        operand = sqrt(operand); 
    }
    else if ([@"+/-" isEqual:operation])
    {
        operand = - operand;
    }
    else if ([@"1/x" isEqual:operation])
    {
        operand = 1/operand;
    }
    else if ([@"sin" isEqual:operation])
    {
        operand =   (double) sin (operand);
        
    }
    else if ([@"cos" isEqual:operation])
    {
        operand =   (double) cos (operand);
        
    }
    else if ([@"Store" isEqual:operation])
    {
        storedOperand = operand;
        int intOperand = (int)storedOperand;
        NSLog(@"storedOperand %d", intOperand); 
        
    }
    else if ([@"Recall" isEqual:operation])
    {
        operand =   storedOperand;
        
    }
    else if ([@"Mem +" isEqual:operation])
    {
        operand =   operand + storedOperand;
        
    }
    else if ([@"C" isEqual:operation])
    {
        operand =   0;
        
    }
    else
    {
        [self performWaitingOperation];
        waitingOperation = operation;
        waitingOperand = operand;
    }
    return operand;
}

- (void)performWaitingOperation
{
    if ([@"+" isEqual:waitingOperation]) {
        operand = waitingOperand + operand; }
    else if ([@"*" isEqual:waitingOperation]) {
        operand = waitingOperand * operand; }
    else if ([@"-" isEqual:waitingOperation]) {
        operand = waitingOperand - operand; }
    else if ([@"/" isEqual:waitingOperation]) {
        if (operand) {
            operand = waitingOperand / operand;
        } 
    }
}
@end
