//
//  CalculatorAppDelegate.h
//  Calculator
//
//  Created by Redmond Militante on 4/27/12.
//  Copyright (c) 2012 Kraftwerking, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
